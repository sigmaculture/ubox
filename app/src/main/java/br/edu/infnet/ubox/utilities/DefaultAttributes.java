package br.edu.infnet.ubox.utilities;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import br.edu.infnet.ubox.repository.Connection;

public class DefaultAttributes {

    private static FirebaseAuth firebaseAuth;
    private static FirebaseUser firebaseUser;

    public static final String USERSDB = "Users";
    public static final String DELIVERYSDB = "Deliverys";
    public static final String ADMINDB = "Admin";
    public static final String ORDERSDB = "Orders";
    public static final String ImagesStorage = "Images";
    public static final String CLIENT = "client";
    public static final String DELIVERY = "delivery";
    public static final String ADMIN = "admin";
    public static final String ORDER = "order";

    public static final int IMAGE_GALLERY_REQUEST = 20;
    public static final int CAMERA_REQUEST_CODE = 228;
    public static final int CAMERA_PERMISSION_REQUEST_CODE = 4192;

    public static final String TEXTVIEW_NAME = "name";
    public static final String TEXTVIEW_DELIVERORDER = "deliverorder";
    public static final String TEXTVIEW_VALUE = "value";

    public static void toastMessage(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void connectionFirebase() {
        firebaseAuth = Connection.getFirebaseAuth();
        firebaseUser = Connection.getFirebaseUser();
    }
}
