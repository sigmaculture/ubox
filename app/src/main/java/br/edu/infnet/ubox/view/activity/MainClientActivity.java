package br.edu.infnet.ubox.view.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import br.edu.infnet.ubox.R;
import br.edu.infnet.ubox.model.ClientModel;
import br.edu.infnet.ubox.utilities.DefaultAttributes;
import br.edu.infnet.ubox.repository.Connection;

public class MainClientActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;
    private NavigationView navigationView;
    private Button btnAddOrder, btnOrderHistory, btnPendingOrders, btnSupport;

    private ClientModel clientModel = new ClientModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.client_main_drawer_layout);

        setDrawer();
        findIds();
        eventClicks();
        clientModel = (ClientModel) getIntent().getSerializableExtra(DefaultAttributes.CLIENT);
    }

    private void setDrawer() {
        Toolbar toolbar = findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        drawer = findViewById(R.id.main_drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.whiteColor));
        toggle.syncState();

        navigationView = findViewById(R.id.main_navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void findIds() {
        btnAddOrder = findViewById(R.id.btnAddOrderViewClient);
        btnOrderHistory = findViewById(R.id.btnOrderHistoryViewClient);
        btnPendingOrders = findViewById(R.id.btnPendingOrders);
        btnSupport = findViewById(R.id.btnSupport);
    }

    private void eventClicks() {
        btnAddOrder.setOnClickListener(addOrder);
        btnOrderHistory.setOnClickListener(orderHistory);
        btnPendingOrders.setOnClickListener(pendingOrders);
    }

    private View.OnClickListener pendingOrders = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), PendingOrdersActivity.class);
            intent.putExtra(DefaultAttributes.CLIENT, clientModel);
            startActivity(intent);
        }
    };

    private View.OnClickListener addOrder = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), RegisterOrderActivity.class);
            intent.putExtra(DefaultAttributes.CLIENT, clientModel);
            startActivity(intent);
        }
    };

    private View.OnClickListener orderHistory = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), OrderHistoryActivity.class);
            intent.putExtra(DefaultAttributes.CLIENT, clientModel);
            startActivity(intent);
        }
    };

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();
        Intent intent;

        switch (itemId) {
            case R.id.home_item:
                intent = getIntent();
                finish();
                startActivity(intent);
                break;
            case R.id.profile_item:
                break;
            case R.id.addOrder_item:
                intent = new Intent(getApplicationContext(), RegisterOrderActivity.class);
                intent.putExtra(DefaultAttributes.CLIENT, clientModel);
                startActivity(intent);
                break;
            case R.id.pending_orders_item:
                intent = new Intent(getApplicationContext(), PendingOrdersActivity.class);
                intent.putExtra(DefaultAttributes.CLIENT, clientModel);
                startActivity(intent);
                break;
            case R.id.order_history_item:
                intent = new Intent(getApplicationContext(), OrderHistoryActivity.class);
                intent.putExtra(DefaultAttributes.CLIENT, clientModel);
                startActivity(intent);
                break;
            case R.id.share_item:
                break;
            case R.id.support_item:
                break;
            case R.id.logout_item:
                Connection.logout();
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                finish();
                break;
        }
        return false;
    }
}
