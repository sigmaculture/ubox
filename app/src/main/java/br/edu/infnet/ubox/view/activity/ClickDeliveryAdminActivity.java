package br.edu.infnet.ubox.view.activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import br.edu.infnet.ubox.R;
import br.edu.infnet.ubox.model.DeliveryDriverModel;
import br.edu.infnet.ubox.utilities.DefaultAttributes;
import br.edu.infnet.ubox.utilities.UrlTasks;

public class ClickDeliveryAdminActivity extends AppCompatActivity {

    private Button btnAccept, btnReject, btnBackMainAdmin;
    private TextView txtNameDelivery, txtCpfDelivery, txtEmailDelivery;
    private ImageView docCarDelivery, cnhDelivery;

    private List<DeliveryDriverModel> listDeliverys = new ArrayList<>();
    //private String cpfDelivery, emailDelivery, renavanDelivery, cnhDelivery;

    private DeliveryDriverModel deliveryDriverModel = new DeliveryDriverModel();

    private DatabaseReference dbReference = FirebaseDatabase.getInstance().getReference();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_click_delivery_admin);

        deliveryDriverModel = (DeliveryDriverModel) getIntent().getSerializableExtra("delivery");

        findIds();
        setClickListener();
        passAttributesInvView();
    }

    private void passAttributesInvView() {
        txtNameDelivery.setText(String.format("%s %s", deliveryDriverModel.getFirstName(), deliveryDriverModel.getLastName()));
        txtEmailDelivery.setText(deliveryDriverModel.getEmail());
        txtCpfDelivery.setText(deliveryDriverModel.getCpf());

        //getImageUrl(cnhDelivery, deliveryDriverModel.getCnh());
        //getImageUrl(docCarDelivery, deliveryDriverModel.getDocumentCar());
    }

    private void getImageUrl(ImageView imageView, String imageUrl){
        Object[] imgTransfer = new Object[2];
        imgTransfer[0] = imageView;
        imgTransfer[1] = imageUrl;
        UrlTasks urlTasks = new UrlTasks();
        urlTasks.execute(imgTransfer);
    }

    private void findIds() {
        txtNameDelivery = findViewById(R.id.nameDeliveryAdmin);
        txtCpfDelivery = findViewById(R.id.cpfDeliveryAdmin);
        txtEmailDelivery = findViewById(R.id.emailDeliveryAdmin);
        docCarDelivery = findViewById(R.id.docCarDeliveryAdmin);
        cnhDelivery = findViewById(R.id.cnhDeliveryAdmin);
        btnAccept = findViewById(R.id.btnAcceptDelivery);
        btnReject = findViewById(R.id.btnRejectDelivery);
        btnBackMainAdmin = findViewById(R.id.btnBackMainAdmin);
    }

    private void setClickListener() {
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbReference.child(DefaultAttributes.DELIVERYSDB).addValueEventListener(approveDelivery);
            }
        });
        btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnBackMainAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private ValueEventListener approveDelivery = new ValueEventListener() {

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {

            for (DataSnapshot deliverys : dataSnapshot.getChildren()) {
                DeliveryDriverModel deliveryDriverModell = deliverys.getValue(DeliveryDriverModel.class);
                listDeliverys.add(deliveryDriverModell);
            }

            for (DeliveryDriverModel dModel : listDeliverys) {
                if (dModel.getId() != null) {

                    if (dModel.getId().equals(deliveryDriverModel.getId())) {
                        deliveryDriverModel.setApproved(true);
                    }
                }
            }

            dbReference.child(DefaultAttributes.DELIVERYSDB).child(deliveryDriverModel.getId()).setValue(deliveryDriverModel);
            DefaultAttributes.toastMessage(getApplicationContext(), getString(R.string.entregador_aprovado_com_sucesso));
            finish();
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
        }
    };
}
