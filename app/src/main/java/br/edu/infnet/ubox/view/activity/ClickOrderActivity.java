package br.edu.infnet.ubox.view.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import br.edu.infnet.ubox.R;
import br.edu.infnet.ubox.model.DeliveryDriverModel;
import br.edu.infnet.ubox.model.OrderModel;
import br.edu.infnet.ubox.utilities.DefaultAttributes;

public class ClickOrderActivity extends AppCompatActivity {

    private GoogleMap map;
    private SupportMapFragment mapFragment;
    private boolean mLocationPermissionGranted;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 0;
    private Location location;
    private MarkerOptions marker;
    private ArrayList<LatLng> latLngs = new ArrayList<>();

    private TextView txtName, txtDescription, txtValue, txtClient, txtDelivery, txtAddressOriginOrder, txtAddressDestinyOrder;
    private Button btnBack, btnAcceptOrder;
    private DeliveryDriverModel deliveryDriverModel;
    private OrderModel orderModel;
    private DatabaseReference dbReference = FirebaseDatabase.getInstance().getReference().child(DefaultAttributes.ORDERSDB);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_click_order);

        findIds();
        eventClicks();

        deliveryDriverModel = (DeliveryDriverModel) getIntent().getSerializableExtra(DefaultAttributes.DELIVERY);
        orderModel = (OrderModel) getIntent().getSerializableExtra(DefaultAttributes.ORDER);

        passAttributesInView();
        mapFragment.getMapAsync(maps);
    }

    private OnMapReadyCallback maps = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap googleMap) {
            map = googleMap;
            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

            String[] latLngOrderOriginSplit = orderModel.getPositionOrigin().split(",");
            String[] latLngOrderDestinySplit = orderModel.getPositionDestiny().split(",");
            LatLng latLngOrderOrigin = new LatLng(Double.parseDouble(latLngOrderOriginSplit[0]), Double.parseDouble(latLngOrderOriginSplit[1]));
            LatLng latLngOrderDestiny = new LatLng(Double.parseDouble(latLngOrderDestinySplit[0]), Double.parseDouble(latLngOrderDestinySplit[1]));

            createMarker(latLngOrderOrigin, "Origem");
            createMarker(latLngOrderDestiny, "Destino");

//            LatLngBounds.Builder builder = new LatLngBounds.Builder();
//            builder.include(latLngOrderOrigin);
//            builder.include(latLngOrderDestiny);
//            LatLngBounds bounds = builder.build();

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLngOrderOrigin)
                    .zoom(14)
                    .bearing(0)
                    .tilt(40)
                    .build();

            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//            getPlaces();
//            googleApiClient.connect();
            getLocation();
        }
    };

    protected void createMarker(LatLng latLng, String title) {
        map.addMarker(new MarkerOptions()
                .position(latLng)
                .title(title));
    }

    private void getPermissionLocation() {
        final LocationManager locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        int permission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);

        if (permission == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
            DefaultAttributes.toastMessage(getApplicationContext(), getString(R.string.permission_granted));
            FusedLocationProviderClient mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                mFusedLocationProviderClient.getLastLocation()
                        .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location loc) {
                                if (loc != null) {
                                    location = loc;
                                }
                            }
                        });
            } else {
                DefaultAttributes.toastMessage(getApplicationContext(), getString(R.string.turn_on_gps));
                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(i);
            }
        } else {
            DefaultAttributes.toastMessage(getApplicationContext(), getString(R.string.permission_denied));
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    private void getLocation() {
        if (location != null) {
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title(getString(R.string.your_position));
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(latLng.latitude, latLng.longitude))
                    .zoom(14)
                    .bearing(0)
                    .tilt(40)
                    .build();

            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        } else {
            getPermissionLocation();
        }
    }

    private void passAttributesInView() {
        txtName.setText(orderModel.getName());
        txtDescription.setText(orderModel.getDescription());
        txtValue.setText(orderModel.getValue());
        txtClient.setText(orderModel.getClientName());
        txtAddressOriginOrder.setText(orderModel.getPositionAddressOrigin());
        txtAddressDestinyOrder.setText(orderModel.getPositionAddressDestiny());
        txtDelivery.setText(orderModel.getDeliveryId() != null ? orderModel.getDeliveryId() : null);
    }

    private void findIds() {
        txtName = findViewById(R.id.txtNameOrder);
        txtDescription = findViewById(R.id.txtDescriptionOrder);
        txtValue = findViewById(R.id.txtValueOrder);
        txtClient = findViewById(R.id.txtClientNameOrder);
        txtDelivery = findViewById(R.id.txtDeliveryIdOrder);
        txtAddressOriginOrder = findViewById(R.id.txtAddressOriginOrder);
        txtAddressDestinyOrder = findViewById(R.id.txtAddressDestinyOrder);
        btnBack = findViewById(R.id.btnBack);
        btnAcceptOrder = findViewById(R.id.btnAcceptOrder);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapClickOrder);
    }

    private void eventClicks() {
        btnBack.setOnClickListener(backToHome);
        btnAcceptOrder.setOnClickListener(acceptOrder);
    }

    private View.OnClickListener acceptOrder = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            orderModel.setDeliveryId(deliveryDriverModel.getId());
            orderModel.setDeliveryName(deliveryDriverModel.getFirstName() + " " + deliveryDriverModel.getLastName());

            Map<String, Object> orderUpdate = new HashMap<>();
            orderUpdate.put(orderModel.getId(), orderModel);
            dbReference.updateChildren(orderUpdate);

            DefaultAttributes.toastMessage(getApplicationContext(), "Você foi adicionado a esta corrida!");
            Intent intent = new Intent(getApplicationContext(), SendingOrderDeliveryActivity.class);
            intent.putExtra(DefaultAttributes.ORDER, orderModel);
            intent.putExtra(DefaultAttributes.DELIVERY, deliveryDriverModel);
            startActivity(intent);
            finish();
        }
    };

    private View.OnClickListener backToHome = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };
}
