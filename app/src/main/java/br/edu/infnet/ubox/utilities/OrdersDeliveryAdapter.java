package br.edu.infnet.ubox.utilities;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import br.edu.infnet.ubox.R;

import static br.edu.infnet.ubox.utilities.DefaultAttributes.TEXTVIEW_DELIVERORDER;
import static br.edu.infnet.ubox.utilities.DefaultAttributes.TEXTVIEW_NAME;
import static br.edu.infnet.ubox.utilities.DefaultAttributes.TEXTVIEW_VALUE;

public class OrdersDeliveryAdapter extends BaseAdapter {

    public ArrayList<HashMap> list;
    Activity activity;

    public OrdersDeliveryAdapter(Activity activity, ArrayList<HashMap> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewTextViews viewTextViews;
        LayoutInflater layoutInflater = activity.getLayoutInflater();

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_available_orders_item, null);
            viewTextViews = new ViewTextViews();
            viewTextViews.txtName = (TextView) convertView.findViewById(R.id.nameOrder_ListViewItem);
            viewTextViews.txtDescription = (TextView) convertView.findViewById(R.id.deliverOrder_ListViewItem);
            viewTextViews.txtValue = (TextView) convertView.findViewById(R.id.valueOrder_ListViewItem);
            convertView.setTag(viewTextViews);
        } else {
            viewTextViews = (ViewTextViews) convertView.getTag();
        }

        HashMap map = list.get(position);
        viewTextViews.txtName.setText((CharSequence) map.get(TEXTVIEW_NAME));
        viewTextViews.txtDescription.setText((CharSequence) map.get(TEXTVIEW_DELIVERORDER));
        viewTextViews.txtValue.setText((CharSequence) map.get(TEXTVIEW_VALUE));

        if (map.get(TEXTVIEW_DELIVERORDER) == null && map.get(TEXTVIEW_VALUE) == null) {
            viewTextViews.txtDescription.setVisibility(View.GONE);
            viewTextViews.txtValue.setVisibility(View.GONE);
        }

        return convertView;
    }

    private class ViewTextViews {
        TextView txtName, txtDescription, txtValue;
    }
}
