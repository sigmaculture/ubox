package br.edu.infnet.ubox.view.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import br.edu.infnet.ubox.R;
import br.edu.infnet.ubox.model.ClientModel;

public class RegisterCrediCardActivity extends AppCompatActivity {

    private RadioButton rbVisa, rbMastercard;
    private Button btnAddCrediCard;
    private EditText editTextCardNumber;

    private ClientModel client;
    //private CrediCardModel crediCardModel;

    private DatabaseReference myDatabaseReference;
    private FirebaseDatabase myFirebaseDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_credi_card);
        myFirebaseDatabase = FirebaseDatabase.getInstance();
        myDatabaseReference = myFirebaseDatabase.getReference();
        FirebaseApp.initializeApp(getApplicationContext());

        findIds();
        getUser();
    }

    private void getUser() {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            client = new ClientModel();
            client = (ClientModel) intent.getSerializableExtra("user");
        }
    }

    private void findIds() {
        rbVisa = findViewById(R.id.rbVisaCard);
        rbMastercard = findViewById(R.id.rbMasterCard);
        editTextCardNumber = findViewById(R.id.editTextCardNumber);
        btnAddCrediCard = findViewById(R.id.btnAddCrediCard);
        btnAddCrediCard.setOnClickListener(addCrediCard);
    }

    private View.OnClickListener addCrediCard = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //crediCardModel = new CrediCardModel();
            client.setNumberCard(editTextCardNumber.getText().toString().trim());

            if (rbVisa.isChecked()) {
                client.setNameCard(rbVisa.getText().toString().trim());
            } else if (rbMastercard.isChecked()) {
                client.setNameCard(rbMastercard.getText().toString().trim());
            } else {
                Toast.makeText(getApplicationContext(), "Ocorreu um erro no tipo de cartão", Toast.LENGTH_SHORT).show();
                return;
            }

            myDatabaseReference.child("Users").child(client.getId()).setValue(client);
            finish();
        }
    };
}
