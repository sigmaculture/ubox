package br.edu.infnet.ubox.utilities.Directions;

import android.content.Context;
import android.os.AsyncTask;

import com.google.android.gms.maps.GoogleMap;

import java.io.IOException;

public class RequestDirectionsTask extends AsyncTask<Object, String, String> {

    private String url;
    private GoogleMap map;
    private Context context;

    @Override
    protected String doInBackground(Object... objects) {

        url = (String) objects[0];
        map = (GoogleMap) objects[1];
        context = (Context) objects[2];
        String responseString = "";
        try {
            responseString = RequestDirectionsUrl.requestDirection(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return responseString;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        //Parse JSON here
        Object[] dataTransfer = new Object[3];
        RequestDirectionsTaskParse taskParse = new RequestDirectionsTaskParse();
        dataTransfer[0] = s;
        dataTransfer[1] = map;
        dataTransfer[2] = context;
        taskParse.execute(dataTransfer);

    }
}
