package br.edu.infnet.ubox.view.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import br.edu.infnet.ubox.R;
import br.edu.infnet.ubox.model.DeliveryDriverModel;
import br.edu.infnet.ubox.repository.Connection;
import br.edu.infnet.ubox.utilities.DefaultAttributes;

public class MainDeliveryActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private NavigationView navigationView;
    private DrawerLayout drawer;
    private Button btnOrdersAvailable, btnOrderHistory, btnPendingOrders, btnSupport;
    private TextView txtMessageApproved;

    private DeliveryDriverModel deliveryDriverModel;

    private DatabaseReference dbReference = FirebaseDatabase.getInstance().getReference().child(DefaultAttributes.DELIVERYSDB);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.delivery_main_drawer_layout);

        DefaultAttributes.connectionFirebase();
        setDrawer();
        findIds();
        eventClicks();

        deliveryDriverModel = (DeliveryDriverModel) getIntent().getSerializableExtra(DefaultAttributes.DELIVERY);
        verifyDelivery();
    }

    private void verifyDelivery() {
        if (!deliveryDriverModel.isApproved()) {
            btnOrderHistory.setClickable(false);
            btnOrdersAvailable.setClickable(false);
            txtMessageApproved.setText("Você ainda não foi aprovado!");
        } else {
            txtMessageApproved.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.bg_button_green));
            txtMessageApproved.setText("Parabéns! Você foi aprovado! :)");
        }
    }

    private View.OnClickListener orderHistory = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), OrderHistoryActivity.class);
            intent.putExtra(DefaultAttributes.DELIVERY, deliveryDriverModel);
            startActivity(intent);
        }
    };

    private void findIds() {
        txtMessageApproved = findViewById(R.id.deliveryMessageApproved);
        btnOrdersAvailable = findViewById(R.id.btnOrdersDeliveryView);
        btnOrderHistory = findViewById(R.id.btnOrderHistoryDelivery);
        btnPendingOrders = findViewById(R.id.btnPendingOrders);
        btnSupport = findViewById(R.id.btnSupport);
    }

    private void eventClicks() {
        btnOrdersAvailable.setOnClickListener(goOrdersAvailable);
        btnOrderHistory.setOnClickListener(orderHistory);
        btnPendingOrders.setOnClickListener(pendingOrders);
    }

    private View.OnClickListener pendingOrders = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), PendingOrdersActivity.class);
            intent.putExtra(DefaultAttributes.DELIVERY, deliveryDriverModel);
            startActivity(intent);
        }
    };

    private View.OnClickListener goOrdersAvailable = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), AvaliableOrdersDeliveryActivity.class);
            intent.putExtra(DefaultAttributes.DELIVERY, deliveryDriverModel);
            startActivity(intent);
        }
    };

    private void setDrawer() {
        Toolbar toolbar = findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        drawer = findViewById(R.id.main_drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.whiteColor));
        toggle.syncState();

        navigationView = findViewById(R.id.main_navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();
        Intent intent;

        switch (itemId) {
            case R.id.home_item:
                intent = getIntent();
                finish();
                startActivity(intent);
                break;
            case R.id.profile_item:
                break;
            case R.id.avaliable_orders_item:
                intent = new Intent(getApplicationContext(), AvaliableOrdersDeliveryActivity.class);
                intent.putExtra(DefaultAttributes.DELIVERY, deliveryDriverModel);
                startActivity(intent);
                break;
            case R.id.pending_orders_item:
                intent = new Intent(getApplicationContext(), PendingOrdersActivity.class);
                intent.putExtra(DefaultAttributes.DELIVERY, deliveryDriverModel);
                startActivity(intent);
                break;
            case R.id.order_history_item:
                intent = new Intent(getApplicationContext(), OrderHistoryActivity.class);
                intent.putExtra(DefaultAttributes.DELIVERY, deliveryDriverModel);
                startActivity(intent);
                break;
            case R.id.share_item:
                break;
            case R.id.support_item:
                break;
            case R.id.logout_item:
                Connection.logout();
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                finish();
                break;
        }
        return false;
    }
}
