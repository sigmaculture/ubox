package br.edu.infnet.ubox.model;

public class RegisterModel {

    private String Email;
    private String Password;
    private String CPassword;

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getCPassword() {
        return CPassword;
    }

    public void setCPassword(String cPassword) {
        CPassword = cPassword;
    }
}
