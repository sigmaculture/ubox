package br.edu.infnet.ubox.model;

import java.io.Serializable;

public class AdminModel implements Serializable {

    private String id;
    private String email;
    private String password;

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
