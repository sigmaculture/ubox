package br.edu.infnet.ubox.model;

import java.io.Serializable;
import java.util.Date;

public class DeliveryDriverModel implements Serializable {

    private String FirstName;
    private String LastName;
    private String Phone;
    private String Cpf;
    private String Birthday;
    private String id;
    private String email;
    private String password;
    private boolean approved;
    private String Cnh;
    private String DocumentCar;
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getCpf() {
        return Cpf;
    }

    public void setCpf(String cpf) {
        Cpf = cpf;
    }

    public String getBirthday() {
        return Birthday;
    }

    public void setBirthday(String birthday) {
        Birthday = birthday;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCnh() {
        return Cnh;
    }

    public void setCnh(String cnh) {
        Cnh = cnh;
    }

    public String getDocumentCar() {
        return DocumentCar;
    }

    public void setDocumentCar(String documentCar) {
        DocumentCar = documentCar;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }
}
