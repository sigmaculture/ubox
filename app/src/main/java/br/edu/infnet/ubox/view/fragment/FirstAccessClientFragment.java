package br.edu.infnet.ubox.view.fragment;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import java.io.Serializable;
import java.util.regex.Pattern;

import br.edu.infnet.ubox.R;
import br.edu.infnet.ubox.model.ClientModel;
import br.edu.infnet.ubox.utilities.DefaultAttributes;
import br.edu.infnet.ubox.utilities.MaskEditUtil;
import br.edu.infnet.ubox.view.activity.SMSCodeAuthActivity;

public class FirstAccessClientFragment extends Fragment {

    protected final Pattern Format_Email = Pattern.compile("^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$");
    protected final Pattern Format_CPF = Pattern.compile("(^\\d{3}\\x2E\\d{3}\\x2E\\d{3}\\x2D\\d{2}$)");
    protected final Pattern Format_Phone = Pattern.compile("(\\(?[\\d]{2,3}\\)?\\s([\\d]{4}\\-[\\d]{4}))");
    protected final Pattern Format_Names = Pattern.compile("([^\\d][\\w])");

    private FirebaseAuth firebaseAuth;
    private String firstname, lastname, cpf, countryPhoneCode, phone, fullNumber, email, password;
    private EditText editFirstnamecup, editLastnamecup, editCpfcup, editBirthdaycup, editPhonecup, editEmailcup, editPasswordCup;
    private Button buttonSalvarcup;
    private TextView txtTermsAndConditions;
    private CheckBox checkTermsAndConditions;

    public FirstAccessClientFragment() {
        firebaseAuth = FirebaseAuth.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_first_access_client, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findIds();
        eventClicks();
    }

    public void findIds() {
        buttonSalvarcup = (Button) getView().findViewById(R.id.buttonSalvarcup);
        editFirstnamecup = (EditText) getView().findViewById(R.id.editFnamecup);
        editLastnamecup = (EditText) getView().findViewById(R.id.editLnamecup);
        editCpfcup = (EditText) getView().findViewById(R.id.editCpfcup);
        editBirthdaycup = getView().findViewById(R.id.editBirthdaycup);
        editPhonecup = (EditText) getView().findViewById(R.id.editPhonecup);
        editEmailcup = (EditText) getView().findViewById(R.id.editEmailcup);
        editPasswordCup = (EditText) getView().findViewById(R.id.editPasswordCup);
        txtTermsAndConditions = getView().findViewById(R.id.txtTermsAndConditions);
        checkTermsAndConditions = getView().findViewById(R.id.checkTermsAndConditions);
        editCpfcup.addTextChangedListener(MaskEditUtil.mask(editCpfcup, MaskEditUtil.FORMAT_CPF));
        editBirthdaycup.addTextChangedListener(MaskEditUtil.mask(editBirthdaycup, MaskEditUtil.FORMAT_DATE));
        editPhonecup.addTextChangedListener(MaskEditUtil.mask(editPhonecup, MaskEditUtil.FORMAT_FONE));
    }

    public boolean checkingInput() {
        boolean onResult = true;
        firstname = editFirstnamecup.getText().toString().trim();
        lastname = editLastnamecup.getText().toString().trim();
        cpf = editCpfcup.getText().toString().trim();
        email = editEmailcup.getText().toString().trim();
        password = editPasswordCup.getText().toString().trim();
        countryPhoneCode = "+55";
        phone = editPhonecup.getText().toString();
        fullNumber = countryPhoneCode + phone;

        if ((firstname.isEmpty() || firstname == null) || firstname.length() < 3 || firstname.matches(Format_Names.toString())) {
            editFirstnamecup.setError("Preencha o campo nome corretamente");
            onResult = false;
        }
        if ((lastname.isEmpty() || lastname == null) || lastname.length() < 3 || lastname.matches(Format_Names.toString())) {
            editLastnamecup.setError("Preencha o campo sobrenome corretamente");
            onResult = false;
        }
        if ((cpf.isEmpty() || cpf == null) || !cpf.matches(Format_CPF.toString())) {
            editCpfcup.setError("Preencha o campo do CPF correntamente \"XXX.XXX.XXX-XX\".");
            onResult = false;
        }
        if (phone.isEmpty() || phone == null) {
            editPhonecup.setError("Preencha o campo Celular corretamente");
        }
        if ((password.isEmpty() || password == null) || password.length() < 6) {
            editPasswordCup.setError("Preencha campo de senha com no minimo 6 digitos.");
            onResult = false;
        }
        if (!checkTermsAndConditions.isChecked()) {
            checkTermsAndConditions.setError("Aceite os termos e condições");
            onResult = false;
        }
        return onResult;
    }

    private void eventClicks() {
        buttonSalvarcup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkingInput()) {

                    ClientModel clientModel = new ClientModel();
                    clientModel.setFirstName(firstname);
                    clientModel.setLastName(lastname);
                    clientModel.setCpf(cpf);
                    clientModel.setEmail(email);
                    clientModel.setPassword(password);
                    clientModel.setPhone(fullNumber);

                    Intent intent = new Intent(getActivity(), SMSCodeAuthActivity.class);
                    intent.putExtra(DefaultAttributes.CLIENT, (Serializable) clientModel);
                    startActivity(intent);
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "Não foi possível continuar com o cadastro", Toast.LENGTH_SHORT).show();
                }
            }
        });
        txtTermsAndConditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
                dialog.setTitle("Termos e Condições");
                dialog.setMessage(getString(R.string.termsAndConditions));
                dialog.setNegativeButton("Fechar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                dialog.create();
                dialog.show();
            }
        });
    }
}
