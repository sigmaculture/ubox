package br.edu.infnet.ubox.view.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;
import java.util.regex.Pattern;

import br.edu.infnet.ubox.R;
import br.edu.infnet.ubox.model.DeliveryDriverModel;
import br.edu.infnet.ubox.utilities.DefaultAttributes;
import br.edu.infnet.ubox.utilities.MaskEditUtil;
import br.edu.infnet.ubox.view.activity.SMSCodeAuthActivity;

import static android.app.Activity.RESULT_OK;
import static br.edu.infnet.ubox.utilities.DefaultAttributes.CAMERA_PERMISSION_REQUEST_CODE;
import static br.edu.infnet.ubox.utilities.DefaultAttributes.CAMERA_REQUEST_CODE;
import static br.edu.infnet.ubox.utilities.DefaultAttributes.IMAGE_GALLERY_REQUEST;


public class FirstAccessDeliveryDriverFragment extends Fragment {

    protected final Pattern Format_Names = Pattern.compile("([^\\d][\\w])");

    private String firstname;
    private String lastname;
    private String cpf;
    private String birthday;
    private String email;
    private String password;
    private String fullNumber;
    private String cnhCamera;
    private String docCarCamera;
    private TextView txtTermsAndConditions;
    private int btnCameraId;
    private EditText editFname, editLname, editCpf, editBirthdaycup, editEmail, editPassword, editPhone;
    private Button buttonSalvar, btnDocCardup, btnCnhdup;
    private CheckBox checkTermsAndConditions;
    private int docCar = 0, cnh = 1;
    private Uri docCarUri, cnhUri;
    private StorageReference storageReference;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_first_access_delivery_driver, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        findIds();
        eventClicks();
    }

    private void findIds() {
        buttonSalvar = getView().findViewById(R.id.buttonSalvardup);
        btnCnhdup = getView().findViewById(R.id.btnCnhdup);
        btnDocCardup = getView().findViewById(R.id.btnDocCardup);
        editFname = getView().findViewById(R.id.editFnamedup);
        editLname = getView().findViewById(R.id.editLnamedup);
        editCpf = getView().findViewById(R.id.editCpfdup);
        editPhone = getView().findViewById(R.id.editPhonedup);
        editEmail = getView().findViewById(R.id.editEmaildup);
        editBirthdaycup = getView().findViewById(R.id.editBirthdaycup);
        editPassword = getView().findViewById(R.id.editPassworddup);
        txtTermsAndConditions = getView().findViewById(R.id.txtTermsAndConditions);
        checkTermsAndConditions = getView().findViewById(R.id.checkTermsAndConditions);
        editCpf.addTextChangedListener(MaskEditUtil.mask(editCpf, MaskEditUtil.FORMAT_CPF));
        editBirthdaycup.addTextChangedListener(MaskEditUtil.mask(editBirthdaycup, MaskEditUtil.FORMAT_DATE));

        storageReference = FirebaseStorage.getInstance().getReference();
    }

    public boolean checkingInput() {
        boolean onResult = true;
        firstname = editFname.getText().toString().trim();
        lastname = editLname.getText().toString().trim();
        cpf = editCpf.getText().toString().trim();
        birthday = editBirthdaycup.getText().toString().trim();
        email = editEmail.getText().toString().trim();
        password = editPassword.getText().toString().trim();
        String countryPhoneCode = "+55";
        String phone = editPhone.getText().toString();
        fullNumber = countryPhoneCode + phone;

        if ((firstname.isEmpty() || firstname == null) || firstname.length() < 3 || firstname.matches(Format_Names.toString())) {
            editFname.setError("Preencha o campo nome corretamente");
            onResult = false;
        }
        if ((lastname.isEmpty() || lastname == null) || lastname.length() < 3 || lastname.matches(Format_Names.toString())) {
            editLname.setError("Preencha o campo sobrenome corretamente");
            onResult = false;
        }
        if ((cpf.isEmpty() || cpf == null) || cpf.length() < 14) {
            editCpf.setError("Preencha o campo do CPF correntamente \"XXX.XXX.XXX-XX\".");
            onResult = false;
        }
        if ((birthday.isEmpty() || birthday == null) || birthday.length() < 10) {
            editBirthdaycup.setError("Preencha o campo de data de nascimento correntamente \"XX/XX/XXXX\".");
            onResult = false;
        }
        if ((cnhCamera.isEmpty() || cnhCamera == null)) {
            btnCnhdup.setError("Adicione a foto da CNH");
            onResult = false;
        }
        if ((docCarCamera.isEmpty() || docCarCamera == null)) {
            btnDocCardup.setError("Adicione a foto do Documento do Carro");
            onResult = false;
        }
        if ((email.isEmpty() || email == null)) {
            editEmail.setError("Preencha com email valido!");
            onResult = false;
        }
        if ((password.isEmpty() || password == null) || password.length() < 6) {
            editPassword.setError("Preencha campo de senha com no minimo 6 digitos.");
            onResult = false;
        }
        if (!checkTermsAndConditions.isChecked()) {
            checkTermsAndConditions.setError("Aceite os termos e condições");
            onResult = false;
        }

        return onResult;
    }

    private View.OnClickListener setImageDelivery = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
            dialogBuilder.setTitle("Escolha a opção");
            dialogBuilder.setPositiveButton("Câmera", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogBuilder, int which) {
                    btnCameraId = v.getId();
                    openCamera();
                }
            });
            dialogBuilder.setNeutralButton("Galeria", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogBuilder, int which) {
                    btnCameraId = v.getId();
                    openGallery();
                }
            });
            dialogBuilder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogBuilder, int which) {
                    dialogBuilder.cancel();
                }
            });

            AlertDialog dialog = dialogBuilder.create();
            Window window = dialog.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();

            wlp.gravity = Gravity.BOTTOM;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            window.setAttributes(wlp);

            dialog.show();
        }
    };

    private void openCamera() {
        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            getCamera();
        } else {
            String[] permissionRequest = {android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
            requestPermissions(permissionRequest, CAMERA_PERMISSION_REQUEST_CODE);
        }
    }

    private void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, IMAGE_GALLERY_REQUEST);
    }

    private void getCamera() {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Uri imageUri;
            Bitmap img;
            if (data != null) {
                switch (requestCode) {
                    case CAMERA_REQUEST_CODE:
                        img = data.getParcelableExtra("data");
                        imageUri = getImageUri(getActivity().getApplicationContext(), img);

                        if (btnCameraId == R.id.btnDocCardup) {
                            docCarCamera = imageUri.toString();
                        } else if (btnCameraId == R.id.btnCnhdup) {
                            cnhCamera = imageUri.toString();
                        }
                        break;
                    case IMAGE_GALLERY_REQUEST:
                        imageUri = data.getData();
                        try {
                            img = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imageUri);
                            imageUri = getImageUri(getActivity().getApplicationContext(), img);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        if (btnCameraId == R.id.btnDocCardup) {
                            docCarCamera = imageUri.toString();
                        } else if (btnCameraId == R.id.btnCnhdup) {
                            cnhCamera = imageUri.toString();
                        }
                        break;
                }
            }
        }
    }

    private Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private void addImageStorage(final int type, String image) {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss", Locale.ENGLISH);
        String uuid = UUID.randomUUID().toString().replace("-", "").substring(1, 5);
        String imageName = simpleDateFormat.format(date) + uuid;

        UploadTask uploadTask;
        StorageReference storageImageRef = storageReference.child(DefaultAttributes.ImagesStorage);
        uploadTask = storageImageRef.child(imageName).putFile(Uri.parse(image));
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                DefaultAttributes.toastMessage(getActivity().getApplicationContext(), e.getMessage());
            }
        });
        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                if (type == 0) {
                    docCarUri = taskSnapshot.getUploadSessionUri();
                    DefaultAttributes.toastMessage(getActivity().getApplicationContext(), String.valueOf(taskSnapshot.getUploadSessionUri()));
                    addUser();
                } else if (type == 1) {
                    cnhUri = taskSnapshot.getUploadSessionUri();
                    DefaultAttributes.toastMessage(getActivity().getApplicationContext(), String.valueOf(taskSnapshot.getUploadSessionUri()));
                    addUser();
                }
            }
        });
    }

    private void addUser() {
        if (cnhUri != null && docCarUri != null) {

            DeliveryDriverModel deliveryDriverModel = new DeliveryDriverModel();
            deliveryDriverModel.setFirstName(firstname);
            deliveryDriverModel.setLastName(lastname);
            deliveryDriverModel.setCpf(cpf);
            deliveryDriverModel.setBirthday(birthday);
            deliveryDriverModel.setPhone(fullNumber);
            deliveryDriverModel.setEmail(email);
            deliveryDriverModel.setPassword(password);
            deliveryDriverModel.setCnh(String.valueOf(cnhUri));
            deliveryDriverModel.setDocumentCar(String.valueOf(docCarUri));

            Intent intent = new Intent(getActivity(), SMSCodeAuthActivity.class);
            intent.putExtra(DefaultAttributes.DELIVERY, deliveryDriverModel);
            startActivity(intent);
        }
    }

    private void eventClicks() {
        buttonSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkingInput()) {
                    addImageStorage(docCar, docCarCamera);
                    addImageStorage(cnh, cnhCamera);

                    addUser();
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "Não foi possível continuar com o cadastro", Toast.LENGTH_SHORT).show();
                }
            }
        });
        txtTermsAndConditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
                dialog.setTitle("Termos e Condições");
                dialog.setMessage(getString(R.string.termsAndConditions));
                dialog.setNegativeButton("Fechar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                dialog.create();
                dialog.show();
            }
        });

        btnCnhdup.setOnClickListener(setImageDelivery);
        btnDocCardup.setOnClickListener(setImageDelivery);
    }
}
