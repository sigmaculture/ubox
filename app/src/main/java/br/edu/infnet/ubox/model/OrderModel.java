package br.edu.infnet.ubox.model;

import java.io.Serializable;
import java.util.Date;

public class OrderModel implements Serializable {

    private String id;
    private String length;
    private String width;
    private String height;
    private String weight;
    private String description;
    private String name;
    private String value;
    private String clientId;
    private String clientName;
    private String deliveryId;
    private String deliveryName;
    private String date;
    private String positionOrigin;
    private String positionAddressOrigin;
    private String positionDestiny;
    private String positionAddressDestiny;
    private boolean delivered;
    private int orderPickUp;

    public int getOrderPickUp() {
        return orderPickUp;
    }

    public void setOrderPickUp(int orderPickUp) {
        this.orderPickUp = orderPickUp;
    }

    public boolean isDelivered() {
        return delivered;
    }

    public void setDelivered(boolean delivered) {
        this.delivered = delivered;
    }

    public String getDeliveryName() {
        return deliveryName;
    }

    public void setDeliveryName(String deliveryName) {
        this.deliveryName = deliveryName;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getPositionAddressOrigin() {
        return positionAddressOrigin;
    }

    public void setPositionAddressOrigin(String positionAddressOrigin) {
        this.positionAddressOrigin = positionAddressOrigin;
    }

    public String getPositionAddressDestiny() {
        return positionAddressDestiny;
    }

    public void setPositionAddressDestiny(String positionAddressDestiny) {
        this.positionAddressDestiny = positionAddressDestiny;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPositionOrigin() {
        return positionOrigin;
    }

    public void setPositionOrigin(String positionOrigin) {
        this.positionOrigin = positionOrigin;
    }

    public String getPositionDestiny() {
        return positionDestiny;
    }

    public void setPositionDestiny(String positionDestiny) {
        this.positionDestiny = positionDestiny;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(String deliveryId) {
        this.deliveryId = deliveryId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
