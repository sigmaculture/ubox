package br.edu.infnet.ubox.view.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


import br.edu.infnet.ubox.R;
import br.edu.infnet.ubox.model.ClientModel;
import br.edu.infnet.ubox.model.DeliveryDriverModel;
import br.edu.infnet.ubox.repository.Connection;
import br.edu.infnet.ubox.repository.UsersDB;
import br.edu.infnet.ubox.utilities.DefaultAttributes;
import br.edu.infnet.ubox.view.adapter.FirstAccessAdapter;

public class LoginActivity extends AppCompatActivity {

    private EditText emailText, senhaText;
    private Button btnSignIn;
    private TextView txtBtnRegister;

    private ClientModel clientModel;
    private DeliveryDriverModel deliveryDriverModel;
    private UsersDB usersDB = new UsersDB();

    private FirebaseAuth auth;
    private FirebaseUser user;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    private void findIds() {
        txtBtnRegister = findViewById(R.id.txtBtnRegister);
        btnSignIn = findViewById(R.id.btnSignIn);
        emailText = findViewById(R.id.editTextLoginEmail);
        senhaText = findViewById(R.id.editTextLoginPassword);
    }

    private void setClickListener() {
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = emailText.getText().toString().trim();
                String senha = senhaText.getText().toString().trim();

                login(email, senha);
            }
        });

        txtBtnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), FirstAccessAdapter.class);
                startActivity(intent);
            }
        });
    }

    private void login(String email, String senha) {
        auth.signInWithEmailAndPassword(email, senha)
                .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            DefaultAttributes.toastMessage(getApplicationContext(), "Login realizado com sucesso");
                            UsersDB.getUser(LoginActivity.this);
                            finish();
                        } else {
                            try {
                                throw task.getException();
                            } catch (FirebaseNetworkException ex) {
                                DefaultAttributes.toastMessage(getApplicationContext(), "Internet não conectada.");
                            } catch (FirebaseAuthInvalidCredentialsException ex) {
                                DefaultAttributes.toastMessage(getApplicationContext(), "Email ou senha incorretos.");
                            } catch (Exception e) {
                                DefaultAttributes.toastMessage(getApplicationContext(), "Ocorreu um erro. Tente novamente.");
                            }
                        }
                    }
                });
    }

    @Override
    protected void onStart() {
        super.onStart();
        auth = Connection.getFirebaseAuth();
        databaseReference = Connection.getDatabaseReference();
        findIds();
        setClickListener();
    }
}
