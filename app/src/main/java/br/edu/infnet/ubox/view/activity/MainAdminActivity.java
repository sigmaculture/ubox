package br.edu.infnet.ubox.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
//import android.support.v7.widget.CardView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import br.edu.infnet.ubox.R;
import br.edu.infnet.ubox.model.DeliveryDriverModel;
import br.edu.infnet.ubox.repository.Connection;
import br.edu.infnet.ubox.utilities.DefaultAttributes;

public class MainAdminActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;
    private NavigationView navigationView;
    private ListView listViewDeliverys;

    private DeliveryDriverModel deliveryDriverModel;
    private List<DeliveryDriverModel> listDeliveries;
    private List<String> listNameDeliveries;
    private String[] listEmpty;

    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private DatabaseReference dbReference = FirebaseDatabase.getInstance().getReference();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_main_drawer_layout);
        DefaultAttributes.connectionFirebase();

        setDrawer();
        findIds();
        eventClicks();

        dbReference.child(DefaultAttributes.DELIVERYSDB).addValueEventListener(getDeliverys);
    }

    private void setDrawer() {
        Toolbar toolbar = findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.main_drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.whiteColor));
        toggle.syncState();

        navigationView = findViewById(R.id.main_navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();

        switch (itemId) {
            case R.id.logout_item:
                Connection.logout();
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                finish();
                break;
        }

        return false;
    }

    private AdapterView.OnItemClickListener clickDelivery = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            DeliveryDriverModel deliveryDriverModel = listDeliveries.get(position);

            Intent intent = new Intent(getApplicationContext(), ClickDeliveryAdminActivity.class);
            intent.putExtra(DefaultAttributes.DELIVERY, deliveryDriverModel);

            startActivity(intent);
        }
    };

    private ValueEventListener getDeliverys = new ValueEventListener() {

        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            deliveryDriverModel = null;
            listDeliveries = new ArrayList<>();
            listNameDeliveries = new ArrayList<>();

            for (DataSnapshot deliveries : dataSnapshot.getChildren()) {
                DeliveryDriverModel deliveryDriverModell = deliveries.getValue(DeliveryDriverModel.class);
                if (!deliveryDriverModell.isApproved()) {
                    listNameDeliveries.add(deliveryDriverModell.getFirstName() + " " + deliveryDriverModell.getLastName());
                    listDeliveries.add(deliveryDriverModell);
                }
            }

            if (listNameDeliveries.isEmpty()) {
                View list_view_item = getLayoutInflater().inflate(R.layout.list_view_item, null);
                ImageView img1 = list_view_item.findViewById(R.id.imageDelivery_ListViewItem);
                ImageView img2 = list_view_item.findViewById(R.id.image_go_ListViewItem);
                img1.setVisibility(View.GONE);
                img2.setVisibility(View.GONE);

                listEmpty = new String[]{"Não há entregadores para aprovar."};

                listViewDeliverys.setOnItemClickListener(null);
                listViewDeliverys.setAdapter(new ArrayAdapter<String>(
                        getApplicationContext(),
                        R.layout.list_view_item,
                        R.id.nameDelivery_ListViewItem, listEmpty));
            } else {
                listViewDeliverys.setAdapter(new ArrayAdapter<String>(
                        getApplicationContext(),
                        R.layout.list_view_item,
                        R.id.nameDelivery_ListViewItem, listNameDeliveries));
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
        }
    };

    private void findIds() {
        listViewDeliverys = findViewById(R.id.listViewDeliverys);
    }

    private void eventClicks() {
        listViewDeliverys.setOnItemClickListener(clickDelivery);
    }
}