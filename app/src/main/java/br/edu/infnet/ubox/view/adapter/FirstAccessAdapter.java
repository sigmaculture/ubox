package br.edu.infnet.ubox.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import br.edu.infnet.ubox.R;
import br.edu.infnet.ubox.view.fragment.FirstAccessClientFragment;
import br.edu.infnet.ubox.view.fragment.FirstAccessDeliveryDriverFragment;

public class FirstAccessAdapter extends AppCompatActivity {

    private ViewPager viewPager;
    private static final int numItem = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_access_adapter);
    }

    private void findIds() {
        viewPager = findViewById(R.id.viewPagerFirstAccessAdapter);
    }


    private void setAdapter() {
        viewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                if (position == 0) {
                    return new FirstAccessClientFragment();
                } else {
                    return new FirstAccessDeliveryDriverFragment();
                }
            }

            @Override
            public int getCount() {
                return numItem;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                if (position == 0) {
                    return "Cliente";
                } else {
                    return "Entregador";
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        findIds();
        setAdapter();
    }
}
