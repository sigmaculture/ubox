package br.edu.infnet.ubox.view.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import br.edu.infnet.ubox.R;
import br.edu.infnet.ubox.model.ClientModel;
import br.edu.infnet.ubox.model.OrderModel;
import br.edu.infnet.ubox.utilities.DefaultAttributes;
import br.edu.infnet.ubox.utilities.Directions.RequestDirectionsTask;
import br.edu.infnet.ubox.utilities.Directions.RequestDirectionsUrl;
import br.edu.infnet.ubox.view.adapter.PlaceAutoCompleteAdapter;


public class RegisterOrderActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 0;
    private boolean mLocationPermissionGranted;

    private EditText editTextLength, editTextWidth, editTextHeight, editTextWeight, editTextDescription, editTextName;
    private Button btnAddOrder;

    private Double length, width, height, weight, distance;
    private String price, priceFinal, description, name;

    private GoogleMap map;
    private SupportMapFragment mapFragment;
    private Geocoder geocoder;
    private Marker marker;
    private Location location;
    private android.location.Address address;
    private android.location.Address addressLocation;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private PlaceAutoCompleteAdapter placeAutocompleteAdapter;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private boolean gps_provider = false, network_provider = false;
    private GoogleApiClient googleApiClient;
    private static final LatLngBounds LAT_LNG_BOUNDS = new LatLngBounds(
            new LatLng(-40, -168), new LatLng(71, 136));

    private AutoCompleteTextView autoCompleteTextView;
    private ImageButton imgSearch;
    private TextView txtValueOrder, txtDistanceOrder;
    private View dialogMap;
    private AlertDialog.Builder dialog;

    private OrderModel orderModel;
    private ClientModel clientModel;
    private ArrayList<OrderModel> orderModelList = new ArrayList<OrderModel>();
    private DatabaseReference dbReference = FirebaseDatabase.getInstance().getReference().child(DefaultAttributes.ORDERSDB);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_order);

        clientModel = (ClientModel) getIntent().getSerializableExtra(DefaultAttributes.CLIENT);
        geocoder = new Geocoder(this, Locale.getDefault());
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        findIds();
        eventClicks();
        getLocationListener();
        getPermissionLocation();
    }

    private OnMapReadyCallback maps = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap googleMap) {
            map = googleMap;
            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            googleApiClient.connect();
            getPermissionLocation();
            getPlaces();
        }
    };

    private void getLocationListener() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            gps_provider = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            network_provider = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            fusedLocationProviderClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location loc) {
                    if (loc != null) {
                        location = loc;
                        getLocation();
                    }
                }
            });

            locationListener = new LocationListener() {
                public void onLocationChanged(Location loc) {
                    location = loc;
                    getLocation();
                }

                public void onStatusChanged(String provider, int status, Bundle extras) {
                }

                public void onProviderEnabled(String provider) {
                }

                public void onProviderDisabled(String provider) {
                    DefaultAttributes.toastMessage(getApplicationContext(), getString(R.string.turn_on_gps));
                    Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(i);
                }
            };
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        } catch (SecurityException ex) {
            DefaultAttributes.toastMessage(this, ex.getMessage());
        }
    }

    private void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            if (location != null && map != null) {
                map.setMyLocationEnabled(true);
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(latLng.latitude, latLng.longitude))
                        .zoom(14)
                        .bearing(0)
                        .tilt(40)
                        .build();

                map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        } catch (SecurityException ex) {
            DefaultAttributes.toastMessage(this, ex.getMessage());
        }
    }

    private void getPermissionLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            getLocation();
        }
    }

    private View.OnClickListener openDialog = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (validateFields(editTextLength, editTextWidth, editTextHeight, editTextWeight, editTextDescription, editTextName)) {
                dialogRegisteredOrder();
            } else {
                DefaultAttributes.toastMessage(getApplicationContext(), "Erro ao adicionar. Reveja os campos e tente novamente.");
            }
        }
    };

    private void dialogRegisteredOrder() {
        if (dialog == null && dialogMap == null) {
            dialog = new AlertDialog.Builder(RegisterOrderActivity.this);
            dialogMap = getLayoutInflater().inflate(R.layout.dialogmap, null);
        }
        dialog.setTitle("Estamos quase lá!");
        dialog.setView(dialogMap);
        findDialogIds();
        mapFragment.getMapAsync(maps);
        getLocation();
        getPlaces();

        dialog.setPositiveButton("Adicionar encomenda", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                addOrder();
            }
        });
        dialog.setNegativeButton("Cancelar encomenda", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                ((ViewGroup) dialogMap.getParent()).removeView(dialogMap);
            }
        });

        dialog.create();
        dialog.show();
    }

    private void getPlaces() {
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient
                    .Builder(this)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .enableAutoManage(this, this)
                    .build();
        }
        placeAutocompleteAdapter = new PlaceAutoCompleteAdapter(this, googleApiClient, LAT_LNG_BOUNDS, null);
        autoCompleteTextView.setAdapter(placeAutocompleteAdapter);
    }

    private View.OnClickListener addDestinyMap = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (routeMap()) {
                convertStringEditTexts();
                calcAttributes(distance);
            }
        }
    };

    private boolean routeMap() {
        boolean result = true;

        if (!autoCompleteTextView.getText().toString().isEmpty() && location != null) {
            map.clear();
            address = null;
            addressLocation = null;

            String searchString = autoCompleteTextView.getText().toString();

            Geocoder geocoder = new Geocoder(RegisterOrderActivity.this);
            List<Address> list = new ArrayList<>();
            List<Address> listAddressLocation = new ArrayList<>();
            try {
                list = geocoder.getFromLocationName(searchString, 1);

                if (location != null) {
                    listAddressLocation = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                }
            } catch (IOException ignored) {
            }

            address = list.get(0);
            addressLocation = listAddressLocation.get(0);
            Location locationDestiny = new Location("Destino");
            locationDestiny.setLatitude(address.getLatitude());
            locationDestiny.setLongitude(address.getLongitude());
            LatLng latLngDestiny = new LatLng(address.getLatitude(), address.getLongitude());

            String url = RequestDirectionsUrl.getRequestUrl(latLngDestiny, location);
            Object[] dataTransfer = new Object[3];
            RequestDirectionsTask getDirectionsData = new RequestDirectionsTask();
            dataTransfer[0] = url;
            dataTransfer[1] = map;
            dataTransfer[2] = getApplicationContext();
            getDirectionsData.execute(dataTransfer);

            map.addMarker(new MarkerOptions().position(latLngDestiny).title(address.getAddressLine(0))
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));

            float distanceFloat = location.distanceTo(locationDestiny);
            distance = (double) distanceFloat / 1000;
        } else {
            DefaultAttributes.toastMessage(getApplicationContext(), "Erro! Espere a localização ativar ou digite o campo de endereço");
            result = false;
        }

        return result;
    }

    private void calcAttributes(Double distance) {
        priceFinal = price(length, width, height, weight, distance);
        String distanceString = String.format("%.2f", distance);

        txtValueOrder.setText(priceFinal);
        txtDistanceOrder.setText(distanceString + " quilômetros");
    }

    private void addOrder() {
        if (!txtValueOrder.getText().toString().isEmpty() && !txtDistanceOrder.getText().toString().isEmpty()) {
            orderModel = new OrderModel();
            String uuid = UUID.randomUUID().toString();
            orderModel.setId(uuid);
            orderModel.setClientId(clientModel.getId());
            orderModel.setClientName(clientModel.getFirstName() + " " + clientModel.getLastName());
            orderModel.setValue(priceFinal);
            orderModel.setName(name);
            orderModel.setDescription(description);
            orderModel.setLength(String.valueOf(length));
            orderModel.setWidth(String.valueOf(width));
            orderModel.setHeight(String.valueOf(height));
            orderModel.setWeight(String.valueOf(weight));
            orderModel.setOrderPickUp(1);
            orderModel.setPositionOrigin(location.getLatitude() + ", " + location.getLongitude());
            orderModel.setPositionAddressOrigin(addressLocation.getAddressLine(0));
            orderModel.setPositionDestiny(address.getLatitude() + ", " + address.getLongitude());
            orderModel.setPositionAddressDestiny(address.getAddressLine(0));
            orderModel.setDelivered(false);

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date date = new Date();
            orderModel.setDate(dateFormat.format(date));

            dbReference.child(uuid).setValue(orderModel);

            DefaultAttributes.toastMessage(getApplicationContext(), "Encomenda cadastrada com sucesso!");
            ((ViewGroup) dialogMap.getParent()).removeView(dialogMap);
            finish();
        } else {
            DefaultAttributes.toastMessage(getApplicationContext(), "Calcule a rota antes de adicionar a encomenda");
            ((ViewGroup) dialogMap.getParent()).removeView(dialogMap);
        }
    }

    private boolean validateFields(EditText editTextLength, EditText editTextWidth, EditText editTextHeight,
                                   EditText editTextWeight, EditText editTextDescription, EditText editTextName) {
        boolean result = true;
        if (editTextLength.getText().toString().isEmpty() ||
                editTextLength.getText().toString().equals("") ||
                editTextLength.getText().toString().contains(";")) {
            editTextLength.setError("Digite um comprimento válido");
            result = false;
        }
        if (editTextWidth.getText().toString().isEmpty() ||
                editTextWidth.getText().toString().equals("") ||
                editTextWidth.getText().toString().contains(";")) {
            editTextWidth.setError("Digite uma largura válida");
            result = false;
        }
        if (editTextHeight.getText().toString().isEmpty() ||
                editTextHeight.getText().toString().equals("") ||
                editTextHeight.getText().toString().contains(";")) {
            editTextHeight.setError("Digite uma altura válida");
            result = false;
        }
        if (editTextWeight.getText().toString().isEmpty() ||
                editTextWeight.getText().toString().equals("") ||
                editTextWeight.getText().toString().contains(";")) {
            editTextWeight.setError("Digite um peso válido");
            result = false;
        }
        if (editTextDescription.getText().toString().isEmpty() || editTextDescription.getText().toString().equals("")) {
            editTextDescription.setError("Digite uma descrição válida");
            result = false;
        }
        if (editTextName.getText().toString().isEmpty() || editTextName.getText().toString().equals("")) {
            editTextName.setError("Digite um nome válido");
            result = false;
        }

        return result;
    }

    private String price(Double length, Double width, Double height, Double weight, Double meters) {
        /*
            Dimensões
            Alt X lar X com
            Cm
            10 cm³ = $1

            Peso
            100g = $1

            Pacote = Peso + Dimensão

            Distância:
            5 km = $2

            Preço base: $5

            Preço final :
            Preço Base + Distância +Pacote
        */
        Double dimens = (length / 100) * (width / 100) * (height / 100);
        Double pack = dimens + weight;

        Double valueDistance = (meters * 5) / 2;

        Double priceDefault = 5.00;
        Double finalValue = priceDefault + pack + valueDistance;
        Locale ptBr = new Locale("pt", "BR");
        price = NumberFormat.getCurrencyInstance(ptBr).format(
                finalValue);

        return price;
    }

    private void convertStringEditTexts() {
        length = Double.parseDouble(editTextLength.getText().toString().trim());
        width = Double.parseDouble(editTextWidth.getText().toString().trim());
        height = Double.parseDouble(editTextHeight.getText().toString().trim());
        weight = Double.parseDouble(editTextWeight.getText().toString().trim());
        description = editTextDescription.getText().toString();
        name = editTextName.getText().toString();
    }

    private void findIds() {
        editTextLength = findViewById(R.id.editTextLength);
        editTextWidth = findViewById(R.id.editTextWidth);
        editTextHeight = findViewById(R.id.editTextHeight);
        editTextWeight = findViewById(R.id.editTextWeight);
        editTextDescription = findViewById(R.id.editTextDescription);
        editTextName = findViewById(R.id.editTextName);
        btnAddOrder = findViewById(R.id.btnAddOrderClient);
    }

    private void findDialogIds() {
        autoCompleteTextView = dialogMap.findViewById(R.id.autoCompleteTxtView);
        imgSearch = dialogMap.findViewById(R.id.imgSearch);
        imgSearch.setOnClickListener(addDestinyMap);
        txtValueOrder = dialogMap.findViewById(R.id.txtValueOrder);
        txtDistanceOrder = dialogMap.findViewById(R.id.txtDistanceOrder);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapDialog);
    }

    private void eventClicks() {
        btnAddOrder.setOnClickListener(openDialog);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getPermissionLocation();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            getPermissionLocation();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        DefaultAttributes.toastMessage(getApplicationContext(), "Erro na api client");
    }

    @Override
    protected void onDestroy() {
        locationManager.removeUpdates(locationListener);
        locationManager = null;
        super.onDestroy();
    }
}