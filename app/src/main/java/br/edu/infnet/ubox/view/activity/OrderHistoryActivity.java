package br.edu.infnet.ubox.view.activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

import br.edu.infnet.ubox.R;
import br.edu.infnet.ubox.model.ClientModel;
import br.edu.infnet.ubox.model.DeliveryDriverModel;
import br.edu.infnet.ubox.model.OrderModel;
import br.edu.infnet.ubox.repository.Connection;
import br.edu.infnet.ubox.repository.UsersDB;
import br.edu.infnet.ubox.utilities.DefaultAttributes;
import br.edu.infnet.ubox.utilities.OrdersDeliveryAdapter;

public class OrderHistoryActivity extends AppCompatActivity {

    private ListView listViewOrdersHistory;

    private DeliveryDriverModel deliveryDriverModel;
    private ClientModel clientModel;
    private OrderModel orderModel;
    private ArrayList<OrderModel> orderModelArrayList = new ArrayList<>();
    private ArrayList<OrderModel> listOrders = new ArrayList<>();
    private ArrayList<HashMap> list = new ArrayList<HashMap>();
    ;

    private UsersDB usersDB = new UsersDB();
    private FirebaseAuth firebaseAuth = Connection.getFirebaseAuth();
    private FirebaseUser firebaseUser = Connection.getFirebaseUser();
    private DatabaseReference dbOrderReference = FirebaseDatabase.getInstance().getReference().child(DefaultAttributes.ORDERSDB);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_order);

        findIds();

        clientModel = (ClientModel) getIntent().getSerializableExtra(DefaultAttributes.CLIENT);
        deliveryDriverModel = (DeliveryDriverModel) getIntent().getSerializableExtra(DefaultAttributes.DELIVERY);

        if (clientModel != null) {
            getUserOrders(clientModel);
        } else if (clientModel == null && deliveryDriverModel != null) {
            getUserOrders(deliveryDriverModel);
        } else {
            DefaultAttributes.toastMessage(getApplicationContext(), "Ocorreu um erro ao carregar as encomendas");
        }
    }

    private <T> void getUserOrders(final T model) {
        dbOrderReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listOrders = new ArrayList<>();

                for (DataSnapshot order : dataSnapshot.getChildren()) {
                    OrderModel oM = order.getValue(OrderModel.class);
                    listOrders.add(oM);
                }
                for (OrderModel oModel : listOrders) {
                    if (model.equals(clientModel) &&
                            oModel.getClientId().equals(firebaseUser.getUid()) &&
                            oModel.isDelivered()) {
                        orderModelArrayList.add(oModel);
                    } else if (oModel.getDeliveryId() != null &&
                            model.equals(deliveryDriverModel) &&
                            oModel.getDeliveryId().equals(firebaseUser.getUid()) &&
                            oModel.isDelivered()) {
                        orderModelArrayList.add(oModel);
                    }
                }
                getOrders(orderModelArrayList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    private void getOrders(ArrayList<OrderModel> arrayList) {

        for (OrderModel orderModel : arrayList) {
            HashMap temp = new HashMap();
            temp.put(DefaultAttributes.TEXTVIEW_NAME, orderModel.getName());
            temp.put(DefaultAttributes.TEXTVIEW_DELIVERORDER, getString(R.string.send));
            temp.put(DefaultAttributes.TEXTVIEW_VALUE, orderModel.getValue());
            list.add(temp);
        }

        if (list.isEmpty()) {
            HashMap temp = new HashMap();
            temp.put(DefaultAttributes.TEXTVIEW_NAME, getString(R.string.list_empty_orders));
            list.add(temp);
        }

        OrdersDeliveryAdapter ordersDeliveryAdapter = new OrdersDeliveryAdapter(this, list);
        listViewOrdersHistory.setAdapter(ordersDeliveryAdapter);
    }

    private void findIds() {
        listViewOrdersHistory = findViewById(R.id.listViewOrdersHistory);
    }
}