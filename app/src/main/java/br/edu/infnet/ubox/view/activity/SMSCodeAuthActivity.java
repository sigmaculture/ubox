package br.edu.infnet.ubox.view.activity;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.security.GeneralSecurityException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import br.edu.infnet.ubox.R;
import br.edu.infnet.ubox.repository.UsersDB;
import br.edu.infnet.ubox.utilities.Cryptography;
import br.edu.infnet.ubox.model.ClientModel;
import br.edu.infnet.ubox.utilities.DefaultAttributes;
import br.edu.infnet.ubox.model.DeliveryDriverModel;

public class SMSCodeAuthActivity extends AppCompatActivity {

    private ClientModel client;
    private DeliveryDriverModel delivery;

    private String phoneVerificationId;
    private ProgressBar progressBar;
    private EditText smsCodeText;
    private Button btnAuthSMSCode;
    private int typeUser; // 0 for user default & 1 for delivery user.

    private static final String TAG = "PhoneAuth";
    private FirebaseAuth firebaseAuth;
    private DatabaseReference myDatabaseReference;
    private FirebaseDatabase myFirebaseDatabase;
    private FirebaseStorage storage;
    private StorageReference storageRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smscode_auth);

        firebaseAuth = FirebaseAuth.getInstance();
        myFirebaseDatabase = FirebaseDatabase.getInstance();
        myDatabaseReference = myFirebaseDatabase.getReference();
        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReference();
        FirebaseApp.initializeApp(getApplicationContext());

        findIds();
        verifyUser();
    }

    private void findIds() {
        smsCodeText = findViewById(R.id.editTextSMSCode);
        progressBar = findViewById(R.id.progressBarSMSCode);
        btnAuthSMSCode = findViewById(R.id.btnAuthSMSCode);
        btnAuthSMSCode.setOnClickListener(authSMSCode);
    }

    //verify if user is client or delivery and attributes int typeUser to differentiate users.
    private void verifyUser() {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            client = new ClientModel();
            client = (ClientModel) intent.getSerializableExtra(DefaultAttributes.CLIENT);
            delivery = new DeliveryDriverModel();
            delivery = (DeliveryDriverModel) intent.getSerializableExtra(DefaultAttributes.DELIVERY);
        }

        if (client != null && delivery == null) {
            typeUser = 0;
            sendTheVerificationCode(client.getPhone());
        } else if (client == null && delivery != null) {
            typeUser = 1;
            sendTheVerificationCode(delivery.getPhone());
        } else {
            DefaultAttributes.toastMessage(getApplicationContext(), "Ocorreu um erro ao analisar os dados. Por favor, tente novamente");
        }
    }

    //send the code for phone number.
    private void sendTheVerificationCode(String phoneNumber) {
        progressBar.setVisibility(View.VISIBLE);
        firebaseAuth.useAppLanguage();
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,
                60,
                TimeUnit.SECONDS,
                this,
                callbacks
        );
    }

    public View.OnClickListener authSMSCode = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String code = smsCodeText.getText().toString().trim();

            if (code.isEmpty() || code.length() < 6) {
                smsCodeText.setError("Digite o Código...");
                smsCodeText.requestFocus();
                return;
            }

            verifyCode(code);
        }
    };

    //verify code of the sms.
    private void verifyCode(String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(phoneVerificationId, code);
        signInWithPhoneAuthCredential(typeUser, credential);
    }

    //create auth in firebase with credentials email&password and sms.
    private void signInWithPhoneAuthCredential(int typeUser, final PhoneAuthCredential credential) {
        if (typeUser == 0) {
            firebaseAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        try {
                            //link credential email&password with sms.
                            AuthCredential authEmailCredential = EmailAuthProvider.getCredential(client.getEmail(), client.getPassword());
                            firebaseAuth.getCurrentUser().linkWithCredential(authEmailCredential);

                            client.setId(task.getResult().getUser().getUid());
                            client.setPassword(Cryptography.encrypt(client.getPassword()));
                            KeeppingData(client.getId(), client);

                            DefaultAttributes.toastMessage(getApplicationContext(), "Cadastro realizado com sucesso.");
                            UsersDB.getUser(SMSCodeAuthActivity.this);
                        } catch (GeneralSecurityException e) {
                            Toast.makeText(SMSCodeAuthActivity.this, "Falha na encriptação", Toast.LENGTH_SHORT).show();
                        }
                    } else if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                        DefaultAttributes.toastMessage(getApplicationContext(), task.getException().getMessage());
                    }
                }
            });
        } else if (typeUser == 1) {
            firebaseAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        try {
                            //link credential email&password with sms.
                            AuthCredential authEmailCredential = EmailAuthProvider.getCredential(delivery.getEmail(), delivery.getPassword());
                            firebaseAuth.getCurrentUser().linkWithCredential(authEmailCredential);

                            delivery.setId(task.getResult().getUser().getUid());
                            delivery.setPassword(Cryptography.encrypt(delivery.getPassword()));
                            delivery.setApproved(false);
                            KeeppingData(delivery.getId(), delivery);

                            DefaultAttributes.toastMessage(getApplicationContext(), "Cadastro realizado com sucesso.");
                            UsersDB.getUser(SMSCodeAuthActivity.this);
                        } catch (GeneralSecurityException e) {
                            Toast.makeText(SMSCodeAuthActivity.this, "Falha na encriptação", Toast.LENGTH_SHORT).show();
                        }
                    } else if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                        DefaultAttributes.toastMessage(getApplicationContext(), task.getException().getMessage());
                    }
                }
            });
        } else {
            DefaultAttributes.toastMessage(getApplicationContext(), "Erro ao completar cadastro e autenticação.");
        }
    }

    //keep image in firebase storage.
    private <T> void imageStorage(T model) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        StorageReference storageUserRef = storageRef;
        byte[] data = new byte[Integer.MAX_VALUE];

        if (model.getClass().equals(ClientModel.class)) {
            storageUserRef = storageRef.child(dateFormat.format(date) + " - " + client.getCpf());
            data = delivery.getCnh().getBytes();
        } else if (model.getClass().equals(DeliveryDriverModel.class)) {
            storageUserRef = storageRef.child(dateFormat.format(date) + " - " + delivery.getCpf());
            data = delivery.getCnh().getBytes();
        }

        UploadTask uploadTask = storageUserRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                DefaultAttributes.toastMessage(getApplicationContext(), "Erro");
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                DefaultAttributes.toastMessage(getApplicationContext(), "Foi");
            }
        });
    }

    public <T> void KeeppingData(String id, T model) {
        if (typeUser == 0) {
            ClientModel clientModel = (ClientModel) model;
            myDatabaseReference.child(DefaultAttributes.USERSDB).child(id).setValue(clientModel);
        } else if (typeUser == 1) {
            DeliveryDriverModel deliveryDriverModel = (DeliveryDriverModel) model;
            myDatabaseReference.child(DefaultAttributes.DELIVERYSDB).child(id).setValue(deliveryDriverModel);
        } else {
            DefaultAttributes.toastMessage(getApplicationContext(), "Não jogou no banco :/");
        }
    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks callbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onVerificationCompleted(PhoneAuthCredential credential) {
            String code = credential.getSmsCode();
            if (code != null) {
                smsCodeText.setText(code);
                verifyCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Log.w(TAG, "onVerificationFailed", e);
            if (e instanceof FirebaseAuthInvalidCredentialsException) {
                DefaultAttributes.toastMessage(getApplicationContext(), "Requisição inválida");
            } else if (e instanceof FirebaseTooManyRequestsException) {
                DefaultAttributes.toastMessage(getApplicationContext(), "A cota de SMS foi excedida");
            }
        }

        @Override
        public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken token) {
            super.onCodeSent(verificationId, token);
            phoneVerificationId = verificationId;
            Log.d(TAG, "onCodeSent:" + phoneVerificationId);
        }
    };
}