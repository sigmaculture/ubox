package br.edu.infnet.ubox.view.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import br.edu.infnet.ubox.R;
import br.edu.infnet.ubox.model.ClientModel;
import br.edu.infnet.ubox.model.DeliveryDriverModel;
import br.edu.infnet.ubox.model.OrderModel;
import br.edu.infnet.ubox.repository.UsersDB;
import br.edu.infnet.ubox.utilities.DefaultAttributes;

public class ClickPendingOrdersActivity extends AppCompatActivity {

    private LinearLayout layoutDelivery;
    private TextView txtNameOrder, txtDateOrder, txtAddressOriginOrder, txtAddressDestinyOrder, txtValueOrder,
            txtNameDelivery, txtPhoneDelivery, txtEmailDelivery;
    private Button btnAcceptDeliveryOrder, btnAuthorizeDeliveryOrder;

    private OrderModel orderModel;
    private ClientModel clientModel;
    private DeliveryDriverModel deliveryDriverModel;
    private DatabaseReference dbOrderReference = FirebaseDatabase.getInstance().getReference().child(DefaultAttributes.ORDERSDB);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_click_pending_orders);

        findIds();
        eventClicks();

        clientModel = (ClientModel) getIntent().getSerializableExtra(DefaultAttributes.CLIENT);
        orderModel = (OrderModel) getIntent().getSerializableExtra(DefaultAttributes.ORDER);
        dbOrderReference.addValueEventListener(checkOrderPickUp);

        passAttributesInView();
    }

    private void passAttributesInView() {
        txtNameOrder.setText(orderModel.getName());
        txtDateOrder.setText(orderModel.getDate());
        txtAddressOriginOrder.setText(orderModel.getPositionAddressOrigin());
        txtAddressDestinyOrder.setText(orderModel.getPositionAddressDestiny());
        txtValueOrder.setText(orderModel.getValue());

        if (orderModel.getDeliveryId() != null) {
            layoutDelivery.setVisibility(View.VISIBLE);

            for (DeliveryDriverModel d : UsersDB.listDeliveries) {
                if (d.getId().equals(orderModel.getDeliveryId())) {
                    deliveryDriverModel = d;
                }
            }

            txtNameDelivery.setText(deliveryDriverModel.getFirstName() + " " + deliveryDriverModel.getLastName());
            txtPhoneDelivery.setText(deliveryDriverModel.getPhone());
            txtEmailDelivery.setText(deliveryDriverModel.getEmail());
        }
    }

    private void findIds() {
        txtNameOrder = findViewById(R.id.txtNameOrder);
        txtDateOrder = findViewById(R.id.txtDateOrder);
        txtAddressOriginOrder = findViewById(R.id.txtAddressOriginOrder);
        txtAddressDestinyOrder = findViewById(R.id.txtAddressDestinyOrder);
        txtValueOrder = findViewById(R.id.txtValueOrder);
        txtNameDelivery = findViewById(R.id.txtNameDelivery);
        txtPhoneDelivery = findViewById(R.id.txtPhoneDelivery);
        txtEmailDelivery = findViewById(R.id.txtEmailDelivery);
        layoutDelivery = findViewById(R.id.layoutDelivery);
        btnAcceptDeliveryOrder = findViewById(R.id.btnAcceptDeliveryOrder);
        btnAuthorizeDeliveryOrder = findViewById(R.id.btnAuthorizeDeliveryOrder);
    }

    private void eventClicks() {
        btnAuthorizeDeliveryOrder.setOnClickListener(authorizeDeliveryOrder);
        btnAcceptDeliveryOrder.setOnClickListener(approvedDelivery);
    }

    private ValueEventListener checkOrderPickUp = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            for (DataSnapshot oMdl : dataSnapshot.getChildren()) {
                OrderModel order = oMdl.getValue(OrderModel.class);

                if(orderModel.getId().equals(order.getId())){

                    if(order.getOrderPickUp() < 2) {
                        btnAuthorizeDeliveryOrder.setVisibility(View.GONE);
                        btnAcceptDeliveryOrder.setVisibility(View.GONE);
                    }else if(order.getOrderPickUp() == 2){
                        DefaultAttributes.toastMessage(getApplicationContext(), "Entregador pediu autorização para enviar a encomenda");
                        btnAuthorizeDeliveryOrder.setVisibility(View.VISIBLE);
                        orderModel = order;
                        return;
                    } else if(order.getOrderPickUp() == 4){
                        DefaultAttributes.toastMessage(getApplicationContext(), "Entregador pediu autorização para entregar a encomenda");
                        btnAuthorizeDeliveryOrder.setVisibility(View.GONE);
                        btnAcceptDeliveryOrder.setVisibility(View.VISIBLE);
                        orderModel = order;
                        return;
                    }

                    if(order.isDelivered()){
                        DefaultAttributes.toastMessage(getApplicationContext(), "Encomenda entregue com sucesso!");
                        Intent i = new Intent(getApplicationContext(), MainClientActivity.class);
                        i.putExtra(DefaultAttributes.CLIENT, clientModel);
                        startActivity(i);
                        finish();
                    }
                }
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
        }
    };

    private View.OnClickListener authorizeDeliveryOrder = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            orderModel.setOrderPickUp(3);
            Map<String, Object> orderUpdate = new HashMap<>();
            orderUpdate.put(orderModel.getId(), orderModel);
            dbOrderReference.updateChildren(orderUpdate);

            DefaultAttributes.toastMessage(getApplicationContext(), "Autorização enviada! Aguarde a entrega no local destinado!");
        }
    };

    private View.OnClickListener approvedDelivery = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            orderModel.setOrderPickUp(5);
            Map<String, Object> orderUpdate = new HashMap<>();
            orderUpdate.put(orderModel.getId(), orderModel);
            dbOrderReference.updateChildren(orderUpdate);
        }
    };
}
