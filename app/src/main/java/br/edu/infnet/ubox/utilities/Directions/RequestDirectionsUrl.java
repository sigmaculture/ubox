package br.edu.infnet.ubox.utilities.Directions;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class RequestDirectionsUrl {

    public static String requestDirection(String reqUrl) throws IOException {
        String responseString = "";
        InputStream inputStream = null;
        HttpURLConnection httpURLConnection = null;
        try {
            URL url = new URL(reqUrl);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.connect();

            //Obter o resultado da resposta
            inputStream = httpURLConnection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            StringBuffer stringBuffer = new StringBuffer();
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                stringBuffer.append(line);
            }

            responseString = stringBuffer.toString();
            bufferedReader.close();
            inputStreamReader.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
            httpURLConnection.disconnect();
        }
        return responseString;
    }

    public static String getRequestUrl(LatLng dest, Location loc) {
        double latitude = loc.getLatitude();
        double longitude = loc.getLongitude();

        //Valor de origem
        String str_org = "origin=" + latitude + "," + longitude;
        //Valor do destino
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        //Valor definido permite o sensor
        String sensor = "sensor=false";
        //Modo para encontrar direção
        String mode = "mode=driving";
        //Construa o parâmetro completo
        String param = str_org + "&" + str_dest + "&" + sensor + "&" + mode;
        //Formato de saída
        String output = "json";
        //Crie o URL para solicitar
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + param;
        return url;
    }
}
