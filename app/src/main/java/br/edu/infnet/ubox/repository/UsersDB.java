package br.edu.infnet.ubox.repository;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import br.edu.infnet.ubox.model.AdminModel;
import br.edu.infnet.ubox.model.ClientModel;
import br.edu.infnet.ubox.model.DeliveryDriverModel;
import br.edu.infnet.ubox.model.OrderModel;
import br.edu.infnet.ubox.utilities.DefaultAttributes;
import br.edu.infnet.ubox.view.activity.MainAdminActivity;
import br.edu.infnet.ubox.view.activity.MainClientActivity;
import br.edu.infnet.ubox.view.activity.MainDeliveryActivity;
import br.edu.infnet.ubox.view.activity.SplashScreenActivity;

public class UsersDB extends Activity {

    private static FirebaseAuth firebaseAuth;
    private static FirebaseUser firebaseUser;
    private static DatabaseReference dbReference = FirebaseDatabase.getInstance().getReference();
    public static ArrayList<ClientModel> listClients = new ArrayList<>();
    public static ArrayList<DeliveryDriverModel> listDeliveries = new ArrayList<>();
    public static ArrayList<OrderModel> listOrders = new ArrayList<>();
    private static AdminModel adminModel = new AdminModel();
    ;
    private static ClientModel clientModel = new ClientModel();
    private static DeliveryDriverModel deliveryDriverModel = new DeliveryDriverModel();
    private static Activity activity;

    private static boolean checkFirebaseUser() {
        firebaseAuth = Connection.getFirebaseAuth();
        firebaseUser = Connection.getFirebaseUser();

        if (firebaseAuth.getInstance().getCurrentUser() != null) {
            return true;
        }

        return false;
    }

    public static void initializeUsers(Activity con) {
        activity = con;
        dbReference.child(DefaultAttributes.USERSDB).addValueEventListener(getClient);
        dbReference.child(DefaultAttributes.DELIVERYSDB).addValueEventListener(getDelivery);
        dbReference.child(DefaultAttributes.ADMINDB).addValueEventListener(getAdmin);
    }

    public static void initializeOrders() {
        dbReference.child(DefaultAttributes.ORDERSDB).addValueEventListener(getOrders);
    }

    //get user logged.
    public static void getUser(Activity activity) {
        checkFirebaseUser();

        if (clientModel.getEmail() != null || deliveryDriverModel.getEmail() != null) {
            clientModel = new ClientModel();
            deliveryDriverModel = new DeliveryDriverModel();
        }

        if (!listClients.isEmpty() && !listDeliveries.isEmpty() && adminModel != null) {
            for (ClientModel cModel : listClients) {
                if (cModel.getId() != null && cModel.getId().equals(firebaseUser.getUid())) {
                    clientModel = cModel;
                }
            }
            for (DeliveryDriverModel dModel : listDeliveries) {
                if (dModel.getId() != null && dModel.getId().equals(firebaseUser.getUid())) {
                    deliveryDriverModel = dModel;
                }
            }
            connectUser(activity);
        }
    }

    public static OrderModel getOrder(String id) {
        OrderModel orderModel = new OrderModel();

        for (OrderModel oModel : listOrders) {
            if (oModel.getId().equals(id)) {
                orderModel = oModel;
            }
        }

        return orderModel;
    }

    private static void connectUser(Activity activity) {
        if (clientModel.getEmail() != null) {
            Intent intent = new Intent(activity, MainClientActivity.class);
            intent.putExtra(DefaultAttributes.CLIENT, clientModel);
            activity.startActivity(intent);
            activity.finish();
        } else if (deliveryDriverModel.getEmail() != null) {
            Intent intent = new Intent(activity, MainDeliveryActivity.class);
            intent.putExtra(DefaultAttributes.DELIVERY, deliveryDriverModel);
            activity.startActivity(intent);
            activity.finish();
        } else if (adminModel.getEmail() != null) {
            Intent intent = new Intent(activity, MainAdminActivity.class);
            intent.putExtra(DefaultAttributes.ADMIN, adminModel);
            activity.startActivity(intent);
            activity.finish();
        } else {
            //DefaultAttributes.toastMessage(activity, "Deu erro");
        }
    }

    private static ValueEventListener getClient = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            for (DataSnapshot clientModels : dataSnapshot.getChildren()) {
                ClientModel client = clientModels.getValue(ClientModel.class);
                listClients.add(client);
            }

            if (checkFirebaseUser() && activity instanceof SplashScreenActivity) {
                getUser(activity);
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
        }
    };

    private static ValueEventListener getDelivery = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            for (DataSnapshot deliverys : dataSnapshot.getChildren()) {
                DeliveryDriverModel deliveryDriverModel = deliverys.getValue(DeliveryDriverModel.class);
                listDeliveries.add(deliveryDriverModel);
            }

            if (checkFirebaseUser() && activity instanceof SplashScreenActivity) {
                getUser(activity);
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
        }
    };

    private static ValueEventListener getAdmin = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            for (DataSnapshot adminModels : dataSnapshot.getChildren()) {
                adminModel = adminModels.getValue(AdminModel.class);
            }

            if (checkFirebaseUser() && activity instanceof SplashScreenActivity) {
                getUser(activity);
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
        }
    };

    private static ValueEventListener getOrders = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            for (DataSnapshot orderModel : dataSnapshot.getChildren()) {
                OrderModel order = orderModel.getValue(OrderModel.class);
                listOrders.add(order);
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
        }
    };

    private static ValueEventListener checkIsPickUp = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };
}
