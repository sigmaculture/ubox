package br.edu.infnet.ubox.utilities.Directions;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.edu.infnet.ubox.utilities.DefaultAttributes;

public class RequestDirectionsTaskParse extends AsyncTask<Object, Void, List<List<HashMap<String, String>>>> {

    private String urlRoutes;
    private GoogleMap map;
    private Context context;

    @Override
    protected List<List<HashMap<String, String>>> doInBackground(Object... objects) {
        JSONObject jsonObject = null;
        List<List<HashMap<String, String>>> routes = null;

        urlRoutes = (String) objects[0];
        map = (GoogleMap) objects[1];
        context = (Context) objects[2];

        try {
            jsonObject = new JSONObject(urlRoutes);
            RequestDirectionsParse directionsParser = new RequestDirectionsParse();
            routes = directionsParser.parse(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return routes;
    }

    @Override
    protected void onPostExecute(List<List<HashMap<String, String>>> lists) {
        //Get list route and display it into the map

        ArrayList points = null;

        PolylineOptions polylineOptions = null;

        for (List<HashMap<String, String>> path : lists) {
            points = new ArrayList();
            polylineOptions = new PolylineOptions();

            for (HashMap<String, String> point : path) {
                double lat = Double.parseDouble(point.get("lat"));
                double lon = Double.parseDouble(point.get("lon"));

                points.add(new LatLng(lat, lon));
            }

            polylineOptions.addAll(points);
            polylineOptions.width(10);
            polylineOptions.color(Color.parseColor("#4169E1"));
            polylineOptions.geodesic(true);
        }

        if (polylineOptions != null) {
            map.addPolyline(polylineOptions);
        }
    }
}
