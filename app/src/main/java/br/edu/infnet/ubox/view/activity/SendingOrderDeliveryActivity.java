package br.edu.infnet.ubox.view.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import br.edu.infnet.ubox.R;
import br.edu.infnet.ubox.model.DeliveryDriverModel;
import br.edu.infnet.ubox.model.OrderModel;
import br.edu.infnet.ubox.utilities.DefaultAttributes;
import br.edu.infnet.ubox.utilities.Directions.RequestDirectionsTask;
import br.edu.infnet.ubox.utilities.Directions.RequestDirectionsUrl;

public class SendingOrderDeliveryActivity extends AppCompatActivity {

    private GoogleMap map;
    private SupportMapFragment mapFragment;
    private Location location;
    private Circle circleOrigin, circleDestiny;
    private CircleOptions circleOptionsOrigin = new CircleOptions(), circleOptionsDestiny = new CircleOptions();
    private LocationManager locationManager;
    private LocationListener locationListener, locationNetworkListener;
    private FusedLocationProviderClient fusedLocationClient;
    private LatLng latLngOrderDestiny;
    private LatLng latLngOrderOrigin;
    private MarkerOptions marker;
    private ArrayList<LatLng> latLngs = new ArrayList<>();
    private LocationCallback locationCallback;
    private boolean mLocationPermissionGranted;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 0;
    private LocationRequest mLocationRequest;

    private int count = 0, count2 = 0, moveCameraLocationCount = 0, approvedOrderCount = 0;
    private boolean gps_provider = false, network_provider = false;
    private Button btnMyLocation, btnRequireOrder, btnDeliverOrder, btnOrigin, btnDestiny;
    private TextView txtDistanceCircle, txtTimeCircle;
    private DeliveryDriverModel deliveryDriverModel;
    private OrderModel orderModel;

    private DatabaseReference dbOrderReference = FirebaseDatabase.getInstance().getReference().child(DefaultAttributes.ORDERSDB);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sending_order_delivery);

        findIds();
        eventClicks();
        deliveryDriverModel = (DeliveryDriverModel) getIntent().getSerializableExtra(DefaultAttributes.DELIVERY);
        orderModel = (OrderModel) getIntent().getSerializableExtra(DefaultAttributes.ORDER);
        dbOrderReference.addValueEventListener(checkApprovedOrder);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mapFragment.getMapAsync(maps);
        getLocationListener();
        getLocationNetworkListener();
        getPermissionLocation();
    }

    //create the map in fragment.
    private OnMapReadyCallback maps = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap googleMap) {
            map = googleMap;
            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

            String[] latLngOrderOriginSplit = orderModel.getPositionOrigin().split(",");
            String[] latLngOrderDestinySplit = orderModel.getPositionDestiny().split(",");
            latLngOrderOrigin = new LatLng(Double.parseDouble(latLngOrderOriginSplit[0]), Double.parseDouble(latLngOrderOriginSplit[1]));
            latLngOrderDestiny = new LatLng(Double.parseDouble(latLngOrderDestinySplit[0]), Double.parseDouble(latLngOrderDestinySplit[1]));

            createMarker(latLngOrderOrigin, "Origem");
            createMarker(latLngOrderDestiny, "Destino");

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLngOrderOrigin)
                    .zoom(18)
                    .bearing(0)
                    .tilt(40)
                    .build();

            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            getPermissionLocation();
        }
    };

    //create one marker in map pass the lat and long & one title.
    protected void createMarker(LatLng latLng, String title) {
        map.addMarker(new MarkerOptions()
                .position(latLng)
                .title(title));
    }

    //create one circle in map pass the circleOptions and lat and long.
    @TargetApi(Build.VERSION_CODES.M)
    protected void createCircle(CircleOptions circleOptions, LatLng latLng) {
        if (checkLocationAndMap()) {
            return;
        }

        circleOptions.center(latLng);
        circleOptions.radius(50);
        circleOptions.fillColor(getColor(R.color.colorPrimaryTransparent));
        circleOptions.strokeColor(getColor(R.color.colorPrimaryDark));
        circleOptions.strokeWidth(3);
        map.addCircle(circleOptions);
    }

    // get the routes of the location for determinated local.
    private void getRoutes(LatLng destinyRoute) {
        String url = RequestDirectionsUrl.getRequestUrl(destinyRoute, location);
        Object[] dataTransfer = new Object[3];
        RequestDirectionsTask getDirectionsData = new RequestDirectionsTask();
        dataTransfer[0] = url;
        dataTransfer[1] = map;
        dataTransfer[2] = getApplicationContext();
        getDirectionsData.execute(dataTransfer);
    }

    //initialize the location listener for check the location of the user by network.
    private void getLocationNetworkListener() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            locationNetworkListener = new LocationListener() {
                public void onLocationChanged(Location loc) {
                    location = loc;
                    getLocation();
                    checkOrder();
                }

                public void onStatusChanged(String provider, int status, Bundle extras) {
                }

                public void onProviderEnabled(String provider) {
                }

                public void onProviderDisabled(String provider) {
                }
            };
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationNetworkListener);
        } catch (SecurityException ex) {
            DefaultAttributes.toastMessage(this, ex.getMessage());
        }
    }

    //initialize the locations listener for check the change location of the user by gps.
    private void getLocationListener() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location loc) {
                    if (loc != null) {
                        location = loc;
                        getLocation();
                        checkOrder();
                    }
                }
            });

            locationListener = new LocationListener() {
                public void onLocationChanged(Location loc) {
                    location = loc;
                    getLocation();
                }

                public void onStatusChanged(String provider, int status, Bundle extras) {
                }

                public void onProviderEnabled(String provider) {
                }

                public void onProviderDisabled(String provider) {
                    DefaultAttributes.toastMessage(getApplicationContext(), getString(R.string.turn_on_gps));
                    Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(i);
                }
            };
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        } catch (SecurityException ex) {
            DefaultAttributes.toastMessage(this, ex.getMessage());
        }
    }

    //check the order is pending, sending or has already been sended.
    private void checkOrder() {
        if (checkLocationAndMap()) {
            return;
        }

        float[] distance;

        if (orderModel.getOrderPickUp() < 3) {

            if (circleDestiny != null) {
                circleDestiny.remove();
            }

            distance = new float[2];
            createCircle(circleOptionsOrigin, latLngOrderOrigin);
            Location.distanceBetween(location.getLatitude(), location.getLongitude(),
                    circleOptionsOrigin.getCenter().latitude, circleOptionsOrigin.getCenter().longitude, distance);

            if (distance[0] > circleOptionsOrigin.getRadius()) {
                getRoutes(latLngOrderOrigin);
                txtDistanceCircle.setText(getString(R.string.dist_ncia) + distance[0] + " metros");
                btnRequireOrder.setEnabled(false);
            } else {
                DefaultAttributes.toastMessage(getApplicationContext(), "Está dentro, acione o botão e peça ao cliente a encomenda");
                btnRequireOrder.setEnabled(true);
            }

        } else if (orderModel.getOrderPickUp() == 3) {

            if (circleOrigin != null) {
                circleOrigin.remove();
            }

            btnRequireOrder.setVisibility(View.GONE);
            btnDeliverOrder.setVisibility(View.VISIBLE);

            distance = new float[2];
            createCircle(circleOptionsDestiny, latLngOrderDestiny);
            Location.distanceBetween(location.getLatitude(), location.getLongitude(),
                    circleOptionsDestiny.getCenter().latitude, circleOptionsDestiny.getCenter().longitude, distance);

            if (distance[0] > circleOptionsDestiny.getRadius()) {
                getRoutes(latLngOrderDestiny);
                txtDistanceCircle.setText(getString(R.string.dist_ncia) + distance[0] + " metros");
                btnDeliverOrder.setEnabled(false);
            } else {
                DefaultAttributes.toastMessage(getApplicationContext(), "Está dentro, acione o botão e entregue a encomenda: " + distance[0]);
                btnDeliverOrder.setEnabled(true);
            }
        } else if (orderModel.getOrderPickUp() == 5) {
            orderModel.setDelivered(true);
            Map<String, Object> orderUpdate = new HashMap<>();
            orderUpdate.put(orderModel.getId(), orderModel);
            dbOrderReference.updateChildren(orderUpdate);

            DefaultAttributes.toastMessage(getApplicationContext(), "Encomenda entregue com sucesso!");
            Intent i = new Intent(getApplicationContext(), MainDeliveryActivity.class);
            i.putExtra(DefaultAttributes.DELIVERY, deliveryDriverModel);
            startActivity(i);
            finish();
        }
    }

    //move position of the camera in map.
    private void moveCameraPosition(LatLng latlng) {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(latlng.latitude, latlng.longitude))
                .zoom(18)
                .bearing(0)
                .tilt(40)
                .build();

        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    //get the location of the user.
    private void getLocation() {
        try {
            if (checkLocationAndMap()) {
                return;
            }

            map.setMyLocationEnabled(true);

            if (moveCameraLocationCount == 0) {
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                moveCameraPosition(latLng);
                moveCameraLocationCount++;
            }

            checkOrder();
        } catch (SecurityException ex) {
            DefaultAttributes.toastMessage(this, ex.getMessage());
        }
    }

    private void getPermissionLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            getLocation();
        }
    }

    //event listener of the database checking if order is approved for send and if order is approved sended.
    private ValueEventListener checkApprovedOrder = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            for (DataSnapshot oMdl : dataSnapshot.getChildren()) {
                OrderModel order = oMdl.getValue(OrderModel.class);

                if (orderModel.getId().equals(order != null ? order.getId() : null)) {
                    if (order.getOrderPickUp() == 3) {
                        DefaultAttributes.toastMessage(getApplicationContext(), "Cliente aprovou o envio da encomenda!");
                        orderModel = order;
                        checkOrder();
                    } else if (order.getOrderPickUp() == 5) {
                        DefaultAttributes.toastMessage(getApplicationContext(), "Cliente aprovou a entrega da encomenda!");
                        orderModel = order;
                        checkOrder();
                    }
                }
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
        }
    };

    //require of the client if the order was delivered.
    private View.OnClickListener deliverOrder = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            orderModel.setOrderPickUp(4);
            Map<String, Object> orderUpdate = new HashMap<>();
            orderUpdate.put(orderModel.getId(), orderModel);
            dbOrderReference.updateChildren(orderUpdate);

            DefaultAttributes.toastMessage(getApplicationContext(), "Pedido de entrega de encomenda feito! Aguarde confirmação do cliente!");
        }
    };

    //require of the client if the order can be sent.
    private View.OnClickListener requireOrder = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            orderModel.setOrderPickUp(2);
            Map<String, Object> orderUpdate = new HashMap<>();
            orderUpdate.put(orderModel.getId(), orderModel);
            dbOrderReference.updateChildren(orderUpdate);

            DefaultAttributes.toastMessage(getApplicationContext(), "Pedido de encomenda feito! Aguarde confirmação do cliente!");
        }
    };

    //move position camera for origin order.
    private View.OnClickListener originCameraPosition = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            moveCameraPosition(latLngOrderOrigin);
        }
    };

    //move position camera for destiny order.
    private View.OnClickListener destinyCameraPosition = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            moveCameraPosition(latLngOrderDestiny);
        }
    };

    //move position camera for location of the user.
    private View.OnClickListener locationCameraPosition = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (checkLocationAndMap()) {
                return;
            }

            LatLng locationLatLng = new LatLng(location.getLatitude(), location.getLongitude());
            moveCameraPosition(locationLatLng);
        }
    };

    //check if variable map and location are null.
    private boolean checkLocationAndMap() {
        return map == null || location == null;
    }

    private void eventClicks() {
        btnMyLocation.setOnClickListener(locationCameraPosition);
        btnOrigin.setOnClickListener(originCameraPosition);
        btnDestiny.setOnClickListener(destinyCameraPosition);
        btnRequireOrder.setOnClickListener(requireOrder);
        btnRequireOrder.setEnabled(false);
        btnDeliverOrder.setOnClickListener(deliverOrder);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
    }

    private void findIds() {
        txtDistanceCircle = findViewById(R.id.txtDistanceCircle);
        txtTimeCircle = findViewById(R.id.txtTimeCircle);
        btnMyLocation = findViewById(R.id.btnMyLocation);
        btnOrigin = findViewById(R.id.btnOrigin);
        btnDestiny = findViewById(R.id.btnDestiny);
        btnRequireOrder = findViewById(R.id.requireOrder);
        btnDeliverOrder = findViewById(R.id.deliverOrder);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getPermissionLocation();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            getPermissionLocation();
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        locationManager.removeUpdates(locationListener);
        locationManager = null;
        super.onDestroy();
    }
}