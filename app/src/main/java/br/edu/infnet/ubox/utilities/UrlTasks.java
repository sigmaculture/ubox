package br.edu.infnet.ubox.utilities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class UrlTasks extends AsyncTask<Object, String, Bitmap> {

    private ImageView imageView;
    private String imgUrl;

    @Override
    protected Bitmap doInBackground(Object... object) {

        imageView = (ImageView) object[0];
        imgUrl = (String) object[1];

        Bitmap btImg = null;
        try {
            URL url = new URL(imgUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true);
            conn.connect();
            InputStream is = conn.getInputStream();
            btImg = BitmapFactory.decodeStream(is);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return btImg;
    }

    @Override
    protected void onPostExecute(Bitmap s) {
        imageView.setImageBitmap(s);
    }
}
