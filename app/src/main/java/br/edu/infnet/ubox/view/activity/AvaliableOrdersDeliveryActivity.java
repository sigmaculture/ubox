package br.edu.infnet.ubox.view.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.edu.infnet.ubox.R;
import br.edu.infnet.ubox.model.DeliveryDriverModel;
import br.edu.infnet.ubox.model.OrderModel;
import br.edu.infnet.ubox.utilities.DefaultAttributes;
import br.edu.infnet.ubox.utilities.OrdersDeliveryAdapter;

public class AvaliableOrdersDeliveryActivity extends AppCompatActivity {

    private ListView listViewOrders;

    private OrderModel orderModel;
    private DeliveryDriverModel deliveryDriverModel;
    private List<OrderModel> listOrders;
    private ArrayList<HashMap> list;
    private List<String> listNameOrders, listDescriptionOrders, listValueOrders;


    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private DatabaseReference dbReference = FirebaseDatabase.getInstance().getReference();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_avaliable_orders_delivery);

        findIds();
        eventClicks();
        deliveryDriverModel = (DeliveryDriverModel) getIntent().getSerializableExtra(DefaultAttributes.DELIVERY);
        dbReference.child(DefaultAttributes.ORDERSDB).addValueEventListener(getOrders);
    }

    private ValueEventListener getOrders = new ValueEventListener() {

        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            listOrders = new ArrayList<>();
            list = new ArrayList<HashMap>();

            for (DataSnapshot oDS : dataSnapshot.getChildren()) {
                OrderModel oModel = oDS.getValue(OrderModel.class);

                if (oModel.getDeliveryId() == null) {
                    HashMap temp = new HashMap();
                    temp.put(DefaultAttributes.TEXTVIEW_NAME, oModel.getName());
                    temp.put(DefaultAttributes.TEXTVIEW_DELIVERORDER, oModel.getDescription());
                    temp.put(DefaultAttributes.TEXTVIEW_VALUE, oModel.getValue());
                    list.add(temp);
                    listOrders.add(oModel);
                }
            }

            OrdersDeliveryAdapter ordersDeliveryAdapter = new OrdersDeliveryAdapter(AvaliableOrdersDeliveryActivity.this, list);
            listViewOrders.setAdapter(ordersDeliveryAdapter);
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
        }
    };

    private AdapterView.OnItemClickListener clickOrder = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            orderModel = listOrders.get(position);

            Intent intent = new Intent(getApplicationContext(), ClickOrderActivity.class);
            intent.putExtra(DefaultAttributes.ORDER, orderModel);
            intent.putExtra(DefaultAttributes.DELIVERY, deliveryDriverModel);
            startActivity(intent);
        }
    };

    private void findIds() {
        listViewOrders = findViewById(R.id.listViewAvailableOrders);
    }

    private void eventClicks() {
        listViewOrders.setOnItemClickListener(clickOrder);
    }
}
